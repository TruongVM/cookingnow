// IDownloadService.aidl
package com.app.cookingnow;

// Declare any non-default types here with import statements

interface IDownloadService {
     void download();
}
