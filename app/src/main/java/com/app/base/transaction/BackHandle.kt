package com.app.base.transaction


interface BackHandle {
    fun actionBack(): Boolean
}