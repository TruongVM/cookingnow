package com.app.base.transaction

import android.support.v4.app.Fragment


interface TransactionManager {

    val fragmentActive: Fragment?
    fun addFragment(fragment: Fragment, isAddBackStack: Boolean = true, styleAnimation: StyleAnimation = StyleAnimation.SLIDE_FROM_RIGHT)

    fun replaceFragment(fragment: Fragment, isAddBackStack: Boolean = true, styleAnimation: StyleAnimation = StyleAnimation.SLIDE_FROM_RIGHT)

    fun goBack(): Boolean

    fun dispose()
}

enum class StyleAnimation {
    SLIDE_FROM_RIGHT, SLIDE_FROM_LEFT, SLIDE_FROM_BOTTOM, SLIDE_NONE
}