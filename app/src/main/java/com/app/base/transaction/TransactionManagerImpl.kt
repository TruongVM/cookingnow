package com.app.base.transaction

import android.annotation.SuppressLint
import android.app.Activity
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import com.app.base.utils.KeyBoardUtils
import com.app.cookingnow.R
import com.app.cookingnow.utils.LogUtils


class TransactionManagerImpl(var activity: Activity?, var fragmentManager: FragmentManager?, var idHolder: Int) : TransactionManager {

    /**
     * Check whether can navigate or not.
     *
     * @return true if can navigate, false otherwise.
     */
    private fun isReadyTransaction(): Boolean = activity != null && activity?.isFinishing == false


    override val fragmentActive: Fragment?
        get() = fragmentManager?.findFragmentById(idHolder)


    override fun addFragment(fragment: Fragment, isAddBackStack: Boolean, styleAnimation: StyleAnimation) {
        if (isReadyTransaction()) {
            KeyBoardUtils.hideKeyboard(activity)
            val fragmentTransaction = fragmentManager?.beginTransaction()
            fragmentTransaction?.let {
                setAnimation(it, styleAnimation)
                it.add(idHolder, fragment)
                if (isAddBackStack)
                    it.addToBackStack(null)
                it.commit()
            }

        }
    }

    override fun replaceFragment(fragment: Fragment, isAddBackStack: Boolean, styleAnimation: StyleAnimation) {
        if (isReadyTransaction()) {
            KeyBoardUtils.hideKeyboard(activity)
            val fragmentTransaction = fragmentManager?.beginTransaction()
            fragmentTransaction?.let {
                setAnimation(it, styleAnimation)
                it.replace(idHolder, fragment)
                if (isAddBackStack)
                    it.addToBackStack(null)
                it.commit()
            }

        }
    }

    override fun goBack(): Boolean {
        if (KeyBoardUtils.hideKeyboard(activity))
            return true
        if (isReadyTransaction() && fragmentManager?.backStackEntryCount ?: 0 > 0) {
            fragmentManager?.popBackStack()
            val transaction = fragmentManager?.beginTransaction()
            val currentFragment = fragmentManager?.findFragmentById(idHolder)
            if (currentFragment != null) {
                transaction?.remove(currentFragment)
                transaction?.commit()
            }
            return true
        }
        return false
    }

    override fun dispose() {
        fragmentManager = null
        activity = null
    }

    @SuppressLint("ResourceType")
    private fun setAnimation(fragmentTransaction: FragmentTransaction, styleAnimation: StyleAnimation) {
        when (styleAnimation) {
            StyleAnimation.SLIDE_FROM_RIGHT -> fragmentTransaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right)
            StyleAnimation.SLIDE_FROM_LEFT -> fragmentTransaction.setCustomAnimations(R.anim.slide_in_from_left, R.anim.slide_out_to_right, R.anim.slide_in_from_right, R.anim.slide_out_to_left)
            StyleAnimation.SLIDE_FROM_BOTTOM -> LogUtils.d(styleAnimation.name)
            else -> LogUtils.d(styleAnimation.name)
        }
    }

}