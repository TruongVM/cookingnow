package com.app.base.activities

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import com.app.base.interfaces.FragmentHost
import com.app.base.transaction.BackHandle
import com.app.base.transaction.TransactionManager
import com.app.base.transaction.TransactionManagerImpl


@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(), FragmentHost {

    private var tm: TransactionManager? = null

    override fun getTransactionManager(): TransactionManager {
        if (tm == null)
            tm = TransactionManagerImpl(this, supportFragmentManager, getPlaceHolder())
        return tm!!
    }

    open fun getPlaceHolder(): Int = 0

    override fun onDestroy() {
        super.onDestroy()
        tm?.dispose()
        tm = null
    }

    override fun onBackPressed() {
        if (!backActionHandle()) {
            super.onBackPressed()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (backActionHandle()) {
                return true
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun backActionHandle(): Boolean {
        var isHandled = false
        val activePage = getTransactionManager().fragmentActive
        if (activePage is BackHandle) {
            isHandled = activePage.actionBack()
        }
        if (!isHandled) isHandled = getTransactionManager().goBack()
        return isHandled
    }
}