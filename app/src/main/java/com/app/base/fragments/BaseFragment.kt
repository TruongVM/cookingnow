package com.app.base.fragments

import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.base.interfaces.ActionBarLayout
import com.app.base.interfaces.FragmentHost
import com.app.base.transaction.BackHandle
import com.app.base.transaction.TransactionManager
import com.app.base.views.AppActionBar
import com.app.cookingnow.R
import com.app.cookingnow.dialog.DialogUtils
import com.app.cookingnow.viewmodels.BaseViewModel


abstract class BaseFragment : Fragment(), BackHandle {
    open var hasActionBar = false
    var actionBar: AppActionBar? = null
    var transactionManager: TransactionManager? = null
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is FragmentHost)
            transactionManager = context.getTransactionManager()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val content: View
        if (hasActionBar) {
            content = inflater.inflate(R.layout.layout_app_action_bar, container, false)
            (content as? ViewGroup)?.addView(inflater.inflate(getContentRes(), container, false))
            actionBar = content.findViewById<AppActionBar>(R.id.appCustomToolbar)
            if (this is ActionBarLayout) {
                actionBar?.init(this)
            }
        } else
            content = inflater.inflate(getContentRes(), container, false)
        return content
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getFragmentViewModel()?.errorCode?.observe(this, Observer {
            if (it != null && it != 0) {
                onApiError(it)
            }
        })
    }

    protected fun onApiError(code: Int) {
        DialogUtils.show(context!!, activity!!.supportFragmentManager, code)
        getFragmentViewModel()?.errorCode?.value = 0
    }

    fun backActionHandle() {
        transactionManager!!.goBack()
    }

    abstract fun getFragmentViewModel(): BaseViewModel?

    @LayoutRes
    abstract fun getContentRes(): Int


}