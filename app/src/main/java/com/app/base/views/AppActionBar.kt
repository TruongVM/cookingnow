package com.app.base.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.app.base.interfaces.ActionBarContent
import com.app.base.interfaces.ActionBarController
import com.app.base.interfaces.ActionBarLayout


class AppActionBar @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), ActionBarContent {
    lateinit var actionBarController: ActionBarController
    override fun init(actionBarContent: ActionBarLayout) {
        actionBarController = actionBarContent.getActionBarController()
        inflateActionBarView()
    }

    private fun inflateActionBarView() {
        View.inflate(context, actionBarController.getActionBarLayout(), this)
        actionBarController.findViews(this)
        actionBarController.setUpViews()
    }

    override fun setTitle(title: String) {
        actionBarController.setTitle(title)
    }

}