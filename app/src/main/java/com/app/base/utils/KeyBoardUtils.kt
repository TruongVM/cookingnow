package com.app.base.utils

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager


object KeyBoardUtils {
    fun hideKeyboard(activity: Activity?): Boolean {
        if (activity != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            try {
                if (imm.isAcceptingText) {
                    return imm.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        return false
    }
}