package com.app.base.interfaces

import com.app.base.transaction.TransactionManager


interface FragmentHost {
    fun getTransactionManager(): TransactionManager
}