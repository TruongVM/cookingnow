package com.app.base.interfaces

import android.support.annotation.LayoutRes
import android.view.ViewGroup


interface ActionBarController {
    @LayoutRes
    fun getActionBarLayout(): Int

    fun findViews(parent: ViewGroup)
    fun setUpViews()
    fun setTitle(title: String)
}