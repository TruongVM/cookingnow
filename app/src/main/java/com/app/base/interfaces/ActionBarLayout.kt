package com.app.base.interfaces

interface ActionBarLayout {
    fun getActionBarController(): ActionBarController
}