package com.app.base.interfaces


interface ActionBarContent {
    fun setTitle(title: String)
    fun init(actionBarContent: ActionBarLayout)
}