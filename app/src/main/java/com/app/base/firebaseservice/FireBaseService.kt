package com.app.base.firebaseservice

import com.app.cookingnow.screens.menuimage.model.ImageMenuModel
import com.app.cookingnow.utils.KeyDatabase
import com.app.cookingnow.utils.LogUtils
import com.google.firebase.database.*

class FireBaseService {

    private var mFireBaseDatabase = FirebaseDatabase.getInstance()
    private lateinit var mDatabaseReference: DatabaseReference

    companion object {
        private var mFireBaseService = FireBaseService()
        fun getInstance(): FireBaseService {
            if (mFireBaseService == null) {
                mFireBaseService = FireBaseService()
            }
            return mFireBaseService
        }

        var listLatestEating = ArrayList<ImageMenuModel>()
        var listImage = ArrayList<ImageMenuModel>()
    }

    fun getDataLatestEating(key: String, listener: FirebaseSuccessListener) {
        mDatabaseReference = mFireBaseDatabase.getReference(key)
        mDatabaseReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(data: DataSnapshot) {
                for (dataSnapshot in data.children) {
                    listLatestEating.add(convertDataToObject(dataSnapshot))
                }
                listener.onGetListLatestSuccess()
            }

            override fun onCancelled(dataError: DatabaseError) {
            }
        })
    }

    fun getDataDetailList(key: String, listener: GetListImageSuccessListener) {
        mDatabaseReference = mFireBaseDatabase.getReference(key)
        mDatabaseReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(data: DataSnapshot) {
                for (dataSnapshot in data.children) {
                    for(datachild in dataSnapshot.children){
                        listImage.add(convertDataToObjectForImageList(datachild))
                    }
                }
                listener.onGetListImageSuccess()
            }

            override fun onCancelled(dataError: DatabaseError) {
            }
        })
    }

    private fun convertDataToObject(dataSnapshot: DataSnapshot): ImageMenuModel {
        val model = ImageMenuModel(dataSnapshot.key!!, dataSnapshot.child(KeyDatabase.KEY_IMAGE_NAME).value.toString(),
                dataSnapshot.child(KeyDatabase.KEY_COUNT_IMAGES).value.toString(), dataSnapshot.child(KeyDatabase.KEY_AVATAR).value.toString())
        return model
    }

    private fun convertDataToObjectForImageList(dataSnapshot: DataSnapshot): ImageMenuModel {
        val model = ImageMenuModel(dataSnapshot.key!!, dataSnapshot.child(KeyDatabase.KEY_IMAGE_NAME).value.toString(),
                dataSnapshot.child(KeyDatabase.KEY_DOWNLOAD_TIMES).value.toString(), dataSnapshot.child(KeyDatabase.KEY_AVATAR).value.toString())
        return model
    }

    fun getListLatestEating(): ArrayList<ImageMenuModel> {
        return listLatestEating
    }

    fun getImageString(position: Int): String {
        return listImage[position].avatar
    }

    fun getListImage(): ArrayList<ImageMenuModel> {
        LogUtils.d(" listImage= "+listImage);
        return listImage
    }

    interface FirebaseSuccessListener {
        fun onGetListLatestSuccess()
    }

    interface GetListImageSuccessListener {
        fun onGetListImageSuccess()
    }

}