package com.app.cookingnow.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider


open class BaseViewModel(app: Application) : AndroidViewModel(app) {
    val isLoading = MutableLiveData<Boolean>()
    val errorCode = MutableLiveData<Int>()

    init {
        isLoading.value = false
        errorCode.value = 0
    }

    fun showLoading(show: Boolean) {
        isLoading.postValue(show)
    }

}

@Suppress("UNCHECKED_CAST")
class AppViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            else -> throw IllegalArgumentException("Unknown ViewModel class")
        }
    }

}