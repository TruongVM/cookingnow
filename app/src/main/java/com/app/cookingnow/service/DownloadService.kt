package com.app.cookingnow.service

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.Environment
import android.os.IBinder
import android.support.annotation.RequiresApi
import android.util.Base64
import android.widget.Toast
import com.app.base.firebaseservice.FireBaseService
import com.app.cookingnow.IDownloadService
import com.app.cookingnow.R
import com.app.cookingnow.activities.MainActivity
import com.app.cookingnow.utils.LogUtils
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class DownloadService : Service() {

    private var CHANNEL_ID = "com.truongvm.mywallpaper"
    private var NOTIFICATION_ID = 1
    private lateinit var mNotification: Notification
    private lateinit var mNotificationManager: NotificationManager

    override fun onBind(intent: Intent?): IBinder {
        LogUtils.d("onBind save service")
        return mBinder
    }

    override fun onCreate() {
        super.onCreate()
        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    fun downloadImage(finalBitmap: Bitmap) {
        val root = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/my_wallpaper")
        myDir.mkdirs()

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val fileName = "my_wallpaper_$timeStamp.jpg"

        val file = File(myDir, fileName)
        LogUtils.d(" file.exists()= " + (file.exists()))
        if (file.exists()) file.delete()
        file.createNewFile()
        try {
            val fileOutputStream = FileOutputStream(file)
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream)
            fileOutputStream.flush()
            fileOutputStream.close()
            Toast.makeText(applicationContext, "Downloaded", Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun convertStringToBitmap(base64image: String): Bitmap {
        val encodeByte = Base64.decode(base64image, Base64.DEFAULT)
        val options = BitmapFactory.Options()
        options.inPurgeable = true
        var image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size, options)

        image = Bitmap.createScaledBitmap(image, 1080, 1920, false)
        LogUtils.d("save image with " + image)
        return image
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(
                channelId,
                channelName, NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    @SuppressLint("NewApi")
    private fun startForegroundService() {
        val pendingIntent: PendingIntent =
                Intent(this, MainActivity::class.java).let { notificationIntent ->
                    PendingIntent.getActivity(this, 0, notificationIntent, 0)
                }

        val channelId =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createNotificationChannel("my_service", "My Background Service")
                } else {
                    ""
                }

        mNotification = Notification.Builder(this, channelId)
                .setContentTitle(getText(R.string.app_name))
                .setContentText(getText(R.string.downloading))
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(pendingIntent)
                .setTicker(getText(R.string.app_name))
                .build()
        startForeground(NOTIFICATION_ID, mNotification)
    }

    private var mBinder = object : IDownloadService.Stub() {

        override fun download() {
            startForegroundService()
            val imageUrl = FireBaseService.getInstance().getImageString(0)
            LogUtils.d("imageurl= " + imageUrl)
            downloadImage(convertStringToBitmap(imageUrl))
        }
    }

}