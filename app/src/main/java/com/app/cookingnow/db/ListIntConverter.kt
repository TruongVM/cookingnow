package com.app.cookingnow.db

import android.arch.persistence.room.TypeConverter


object ListIntConverter {
    @TypeConverter
    @JvmStatic
    fun fromListInt(value: List<Int>?): String? {
        return value?.joinToString(",")
    }

    @TypeConverter
    @JvmStatic
    fun fromString(value: String?): List<Int>? {
        val listValue = value?.split(",")
        return listValue?.map {
            it.trim().toInt()
        }
    }
}