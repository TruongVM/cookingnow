package com.app.cookingnow.db


object DB {
    const val NAME = "smile_db"
    const val VERSION = 1

}

object Table {
    const val USER = "user"
    const val CHILD = "child"
    const val UPLOAD_FILE = "upload_file"
}