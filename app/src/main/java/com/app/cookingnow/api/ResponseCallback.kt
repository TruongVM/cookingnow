package com.app.cookingnow.api

import android.os.Handler
import android.os.Looper
import com.app.cookingnow.api.response.BaseResponse
import com.app.cookingnow.utils.LogUtils
import com.app.cookingnow.viewmodels.BaseViewModel
import retrofit2.Call
import retrofit2.Callback

import retrofit2.Response


abstract class ResponseCallback<T>(private val viewModel: BaseViewModel? = null, private var showLoading: Boolean = true) : Callback<BaseResponse<T>> {
    private val handler = Handler(Looper.getMainLooper())

    init {
        onStartRequest()
    }

    open fun onStartRequest() {
        if (showLoading)
            viewModel?.showLoading(true)
    }

    open fun onFinishRequest() {
        if (showLoading)
            viewModel?.showLoading(false)
    }

    override fun onFailure(call: Call<BaseResponse<T>>?, t: Throwable?) {
        handler.post { onFailed(ResponseCode.CONNECTION_ERROR) }
        onFinishRequest()
    }

    override fun onResponse(call: Call<BaseResponse<T>>?, response: Response<BaseResponse<T>>?) {
        val body = response?.body()
        if (body != null) {
            if (body.code == ResponseCode.SUCCESS) {
                onSuccessInBackground(body.data)
                handler.post { onSuccess(body.data) }
            } else
                handler.post { onFailed(body.code) }
        } else
            handler.post { onFailed(ResponseCode.UNKNOW_ERROR) }
        onFinishRequest()
    }

    protected abstract fun onSuccess(data: T?)
    protected open fun onSuccessInBackground(data: T?) {}
    protected open fun onFailed(code: Int) {
        viewModel?.errorCode?.value = code
        LogUtils.e("Response failed with code $code")
    }
}