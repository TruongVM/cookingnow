package com.app.cookingnow.api.response

import com.google.gson.annotations.SerializedName


class UploadFileResponse(
        @SerializedName("isExist")
        val isExist: Boolean,
        @SerializedName("filePath")
        val filePath: String?,
        @SerializedName("fileHost")
        val fileHost: String?

)