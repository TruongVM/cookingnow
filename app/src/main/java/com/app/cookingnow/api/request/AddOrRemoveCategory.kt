package com.app.cookingnow.api.request

import com.google.gson.annotations.SerializedName


class AddOrRemoveCategory(@SerializedName("categoryNumber") val categoryNumber: Int)