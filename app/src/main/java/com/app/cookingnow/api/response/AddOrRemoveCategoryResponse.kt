package com.app.cookingnow.api.response

import com.google.gson.annotations.SerializedName


class AddOrRemoveCategoryResponse(@SerializedName("userId") val userId: String,
                                  @SerializedName("categories") val categories: List<Int>?)