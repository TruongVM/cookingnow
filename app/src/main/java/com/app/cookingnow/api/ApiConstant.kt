package com.app.cookingnow.api


object ResponseCode{
    const val SUCCESS                         = 0
    const val UNKNOW_ERROR                    = 1
    const val WRONG_DATA_FORMAT               = 2
    const val INVALID_TOKEN                   = 3
    const val TOKEN_EXPIRED                   = 4
    const val INVALID_ACCOUNT                 = 5
    const val EMAIL_NOT_FOUND                 = 6
    const val INVALID_EMAIL                   = 7
    const val NEWEMAIL_IS_EXIST               = 8
    const val OLDPASSWORD_INCORRECT           = 9
    const val INVALID_USER_NAME               = 10
    const val INVALID_BIRTHDAY                = 11
    const val UNUSABLE_EMAIL                  = 12
    const val DUPLICATE_USER_NAME             = 13
    const val FACEBOOK_ID_IS_EXIST            = 14
    const val FACEBOOK_ID_NOT_REGISTED        = 15
    const val INCORRECT_PASSWORD              = 16
    const val INVALID_PASSWORD                = 17
    const val UPLOAD_IMAGE_ERROR              = 19
    const val UPLOAD_FILE_ERROR               = 20
    const val ACCESS_DENIED                   = 21
    const val WRONG_VERIFICATION_CODE         = 22
    const val USER_NOT_EXIST                  = 23
    const val LOCKED_USER                     = 24
    const val LOCK_FEATURE                    = 25
    const val NOT_ENOUGHT_POINT               = 26
    const val AUTHEN_CODE_EXPIRED             = 32
    const val USER_CATEGORY_NOT_ADD           = 112
    const val USER_CATEGORY_OF_CHILDREN       = 114

    const val CONNECTION_ERROR                = 1001
}