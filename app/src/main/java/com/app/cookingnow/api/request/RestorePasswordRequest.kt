package com.app.cookingnow.api.request

import com.app.cookingnow.BuildConfig
import com.app.cookingnow.MainApp
import com.app.cookingnow.preferences.Pref
import com.app.cookingnow.preferences.PrefKey
import com.app.cookingnow.preferences.get
import com.app.cookingnow.utils.Constant
import com.app.cookingnow.utils.Utils
import com.google.gson.annotations.SerializedName

class RestorePasswordRequest(
        @SerializedName("newPassword")
        val newPassword:String,
        @SerializedName("deviceType")
        var deviceType: Int = Constant.DEVICE_TYPE,
        @SerializedName("deviceId")
        var deviceId: String = Utils.getDeviceId(MainApp.instance),
        @SerializedName("deviceName")
        var deviceName: String = Utils.deviceName,
        @SerializedName("applicationVersion")
        var applicationVersion: String = BuildConfig.VERSION_NAME,
        @SerializedName("pushNotificationToken")
        var pushNotificationToken: String = Pref.instance()[PrefKey.PUSH_TOKEN, ""]!!,
        @SerializedName("applicationName")
        var applicationName: String = Constant.APP_NAME
)