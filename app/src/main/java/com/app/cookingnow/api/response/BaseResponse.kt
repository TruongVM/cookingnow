package com.app.cookingnow.api.response

import com.app.cookingnow.api.ResponseCode
import com.google.gson.annotations.SerializedName


class BaseResponse<T> {
    @SerializedName("code")
    var code: Int = ResponseCode.SUCCESS
    @SerializedName("message")
    var message: String? = null
    @SerializedName("data")
    var data: T? = null
}