package com.app.cookingnow.api.request

import com.app.cookingnow.utils.Constant
import com.google.gson.annotations.SerializedName


class GetVerifyCodeRequest(
        @SerializedName("email")
        var email: String,
        @SerializedName("applicationName")
        var applicationName: String = Constant.APP_NAME
)