package com.app.cookingnow.extensions

import android.net.Uri
import android.widget.ImageView
import com.app.cookingnow.MainApp as app

/**
 * Created by NTH1991 on 6/21/2018.
 */
fun ImageView.loadUrl(url: String?) {
    app.instance.picasso.load(url).into(this)
}
fun ImageView.loadUri(uri: Uri?) {
    app.instance.picasso.load(uri).into(this)
}