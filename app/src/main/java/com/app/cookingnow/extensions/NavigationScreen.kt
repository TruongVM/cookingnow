package com.app.cookingnow.extensions

import android.app.Activity
import android.content.Intent
import kotlin.reflect.KClass

/**
 * Created by NTH1991 on 6/22/2018.
 */
fun <T : Activity> KClass<T>.start(activity: Activity, finish: Boolean = false) {
    Intent(activity, this.java).apply {
        activity.startActivity(this)
    }
    if (finish) {
        activity.finish()
    }
}