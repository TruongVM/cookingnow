package com.app.cookingnow.utils

import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.support.annotation.RawRes
import android.text.TextUtils
import android.webkit.MimeTypeMap
import com.app.cookingnow.MainApp
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.net.URLConnection


object UriUtils {
    fun getRealPathFromURI(context: Context, contentUri: Uri): String? {
        if (ContentResolver.SCHEME_FILE == contentUri.scheme) return contentUri.path
        var cursor: Cursor? = null
        try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = context.contentResolver.query(contentUri, proj, null, null, null)
            if (cursor == null) return null
            val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(columnIndex)
        } finally {
            cursor?.close()
        }
    }

    fun getMimeTypeUri(context: Context, uri: Uri): String? {
        var type: String? = null
        if (ContentResolver.SCHEME_CONTENT == uri.scheme)
            type = context.contentResolver.getType(uri)

        if (TextUtils.isEmpty(type)) {
            val mimeTypeMap = MimeTypeMap.getSingleton()
            val extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())
            if (!TextUtils.isEmpty(extension))
                type = mimeTypeMap.getMimeTypeFromExtension(extension.toLowerCase())
        }
        if (TextUtils.isEmpty(type)) {
            try {
                type = getMimeType(context.contentResolver.openInputStream(uri))
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        return type
    }

    @Throws(IOException::class)
    fun getMimeType(stream: InputStream?): String? {
        var inputStream = stream
        try {
            if (inputStream !is BufferedInputStream) {
                inputStream = BufferedInputStream(inputStream!!)
            }
            return URLConnection.guessContentTypeFromStream(inputStream)
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            inputStream!!.close()
        }
        return null
    }

    fun getUriFromResource(@RawRes res: Int): Uri {
        val uri = "android.resource://" + MainApp.instance.packageName + "/" + res
        return Uri.parse(uri)
    }

    fun getFileName(contentResolver: ContentResolver, uri: Uri): String? {
        var result: String? = null
        if (ContentResolver.SCHEME_CONTENT == uri.scheme) {
            contentResolver.query(uri, null, null, null, null)?.use { cursor ->
                if (cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result?.substring(cut + 1)
            }
        }
        return result
    }

    fun getFileSize(contentResolver: ContentResolver, uri: Uri): Long {
        if (uri.scheme == "content") {
            contentResolver.query(uri, null, null, null, null)?.use { cursor ->
                if (cursor.moveToFirst()) {
                    return cursor.getLong(cursor.getColumnIndex(OpenableColumns.SIZE))
                }
            }
        }
        return 0
    }
}