package com.app.cookingnow.utils

import android.text.TextUtils
import java.util.regex.Pattern


object StringUtils {
    fun clearStartEndWhiteSpaces(s: String): String {
        var s = s
        s = s.trim { it <= ' ' }
        while (s.isNotEmpty() && s[0] == '　') {
            s = s.substring(1)
        }
        while (s.isNotEmpty() && s[s.length - 1] == '　') {
            s = s.substring(0, s.length - 1)
        }
        return s
    }

    fun isEmpty(s: String): Boolean {
        return TextUtils.isEmpty(s) || TextUtils.isEmpty(clearStartEndWhiteSpaces(s))
    }

    fun hasSpecialCharacters(string: String): Boolean {
        val p = Pattern.compile(Constant.SPECIAL_CHARACTERS_PATTERN)
        val m = p.matcher(string)
        return m.find()
    }
}