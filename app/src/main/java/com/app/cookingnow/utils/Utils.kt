package com.app.cookingnow.utils

import android.content.Context
import android.hardware.Camera
import android.os.Build
import android.os.Looper
import android.provider.Settings
import android.telephony.TelephonyManager

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

/**
 * All common of utils.
 */
object Utils {

    val isCameraUseByOtherApp: Boolean
        get() {
            var camera: Camera? = null
            try {
                camera = Camera.open()
            } catch (e: RuntimeException) {
                return true
            } finally {
                camera?.release()
            }
            return false
        }

    val deviceName: String
        get() = Build.MANUFACTURER + " " + Build.MODEL

    /**
     * Md5
     *
     * @param input
     * @return data after md5
     */
    fun md5(input: String): String {
        val MD5 = "MD5"
        try {
            // Create MD5 Hash
            val digest = MessageDigest.getInstance(MD5)
            digest.update(input.toByteArray())
            val messageDigest = digest.digest()

            // Create Hex String
            val hexString = StringBuilder()
            for (aMessageDigest in messageDigest) {
                var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
                while (h.length < 2)
                    h = "0$h"
                hexString.append(h)
            }
            return hexString.toString()

        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        return ""
    }

    /**
     * Check sim card in device.
     *
     * @param context
     * @return true if sim card exist. false otherwise.
     */
    fun isSimReady(context: Context): Boolean {
        val telephonyManager = context
                .getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        return if (telephonyManager.simState == TelephonyManager.SIM_STATE_READY) {
            true
        } else false
    }

    /**
     * @param context
     * @return deviceId
     */
    fun getDeviceId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver,
                Settings.Secure.ANDROID_ID)
    }

    fun ensureOnMainThread() {
        if (Looper.myLooper() != Looper.getMainLooper())
            throw IllegalStateException(
                    "This method must be called from the UI thread.")
    }

    fun convertDpToPx(context: Context, dp: Int): Float {
        return context.resources.displayMetrics.density * dp
    }

    fun getDimensionInDp(context: Context, resId: Int): Int {
        val resources = context.resources
        return (resources.getDimension(resId) / resources.displayMetrics.density).toInt()
    }
}
