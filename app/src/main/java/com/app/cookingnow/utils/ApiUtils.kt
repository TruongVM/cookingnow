package com.app.cookingnow.utils

import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.RequestBody


object ApiUtils {
    fun makeApiBody(data: Any): RequestBody {
        val value = Gson().toJson(data)
        return RequestBody.create(MediaType.parse("application/json"), value)
    }
}