package com.app.cookingnow.utils


object Constant {
    //Common
    const val APP_NAME = "cookingnow"
    const val DEVICE_TYPE = 2

    // Pattern param
    const val EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    const val PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})"
    const val USERNAME_PATTERN = "^[a-z0-9_-]{3,15}$"
    const val SPECIAL_CHARACTERS_PATTERN = "[^a-zA-Zぁ-ゖ゙ ゙-゜ァ-ヺＡ-Ｚａ-ｚｧ-ﾟ一-龯㐀-䶵 　]"

    //Time Format
    const val DEFAULT_FORMAT = "yyyy/MM/dd"
    const val TOP_FORMAT = "最終更新:yyyy/MM/dd hh:mm"

    //PERMISSION
    const val REQUEST_PERMISSION = 1010

    //Request Code
    const val REQUEST_TAKE_IMAGE = 100
}

object TopTabs {
    const val TOP = "top"
    const val TIME_LINE = "timeline"
    const val NOTICE = "notice"
    const val MY_PAGE = "mypage"
    const val TOP_ACTION_BAR = "top_action_bar"
    const val TIME_LINE_ACTION_BAR = "timline_action_bar"
    const val NOTICE_ACTION_BAR = "notice_action_bar"
    const val MY_PAGE_ACTION_BAR = "my_page_action_bar"
}

object Action {
    //Action Bar Action
    const val DONE = 1
    const val OPEN_CHAT = 2
}

object TimelinePages {
    const val FOLLOWER = 1
    const val JOINING_CATEGORY = 2
    const val MY_POST = 3
}


object KeyDatabase {
    val KEY_TOP_EATING = "topeating"
    val KEY_LATEST_EATING = "latesteating"
    val KEY_LIST_IMAGE = "list_image"
    val KEY_AVATAR = "avatar"
    val KEY_IMAGE_NAME = "image_name"
    val KEY_COOK_TIME = "cook_time"
    val KEY_COUNT_IMAGES = "count_images"
    val KEY_DOWNLOAD_TIMES = "download_times"
    val KEY_DESCRIPTION = "description"
    val KEY_MANI_RESOURCE = "main_resource"
    val KEY_COUNT_RESOURCE = "count_times"
    val KEY_DIFFICULT_RATE = "difficult_rate"
    val KEY_RESOURCE = "resource"
    val KEY_FOOD_DETAIL_FOOD_RESOURCE_COUNT = "key_top_detail_food_resource_count"
    val KEY_FOOD_DETAIL_FOOD_COOK_TIME = "key_top_detail_food_cook_time"
    val KEY_FOOD_DETAIL_FOOD_DIFFICULT_RATE = "key_top_detail_food_difficult_rate"
    val KEY_IMAGE_LIST_IMAGE_AVATAR = "key_image_list_image_avatar"
    val KEY_IMAGE_LIST_IMAGE_NAME = "key_image_list_image_name"
    val KEY_IMAGE_LIST_IMAGE_DOWN_TIMES = "key_image_list_image_down_times"
    val KEY_FOOD_DETAIL_FOOD_DESCRIPTION = "key_top_detail_food_description"
    val KEY_FOOD_DETAIL_FOOD_RESOURCE = "key_top_detail_food_resource"
}
