package com.app.cookingnow.utils

import android.os.Environment
import android.text.TextUtils
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*


object FileUtils {
    private val APP_NAME = "cookingnow"

    fun createFile(parentDirectory: String, name: String): File {
        val external = Environment.getExternalStorageDirectory()
        val parent: File
        if (TextUtils.isEmpty(parentDirectory))
            parent = external
        else
            parent = File(external, parentDirectory)
        if (!parent.exists())
            parent.mkdirs()
        return File(parent, name)
    }

    fun createAppImageFile(): File {
        val sdf = SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
        val fileName = sdf.format(Calendar.getInstance().time) + ".jpg"
        return createFile(APP_NAME, fileName)
    }

    private val hexDigits = "0123456789abcdef".toCharArray()

    @Throws(IOException::class)
    fun md5(stream: InputStream): String {
        var md5 = ""

        try {
            val bytes = ByteArray(1024)
            var read: Int
            val digest = MessageDigest.getInstance("MD5")
            do {
                read = stream.read(bytes)
                if (read != -1)
                    digest.update(bytes, 0, read)
            } while (read != -1)

            val messageDigest = digest.digest()

            val sb = StringBuilder(32)

            for (b in messageDigest) {
                sb.append(hexDigits[(b.toInt() shr 4) and 0x0f])
                sb.append(hexDigits[b.toInt() and 0x0f])
            }

            md5 = sb.toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return md5
    }
}