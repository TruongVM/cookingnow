package com.app.cookingnow.screens.detailimage.presenter

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import com.app.base.firebaseservice.FireBaseService
import com.app.cookingnow.IDownloadService
import com.app.cookingnow.screens.detailimage.view.IDetailImageView
import com.app.cookingnow.service.DownloadService
import com.app.cookingnow.utils.ImageUtil
import com.app.cookingnow.utils.KeyDatabase


class DetailImagePresenter(var bundle: Bundle, var iDetailImageView: IDetailImageView, var context: Context) : IDetailImagePresenter {
    private lateinit var imageUrl: String

    private lateinit var mDownLoadService: IDownloadService

    override fun onCreate() {
        bundle.let {
            imageUrl = ImageUtil.resizeImage(it.getString(KeyDatabase.KEY_AVATAR)!!)
            displayFullImage(imageUrl)
        }
    }

    override fun onStop() {
    }

    private fun startDownloadService() {
        val intent = Intent(context, DownloadService::class.java)
        context.bindService(intent, mConnection, Context.BIND_AUTO_CREATE)
    }

    fun displayFullImage(url: String) {
        iDetailImageView.displayFullImage(url)
    }

    override fun downloadImage() {
        val downloadAsyncTask = DownLoadAsyncTask(context)
        downloadAsyncTask.execute()
    }


    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            mDownLoadService = IDownloadService.Stub.asInterface(service)

        }

        override fun onServiceDisconnected(name: ComponentName?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }

}