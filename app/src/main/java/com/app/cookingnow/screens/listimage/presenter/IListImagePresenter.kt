package com.app.cookingnow.screens.listimage.presenter

interface IListImagePresenter {
    fun onViewCreated()

    fun loadData()
}