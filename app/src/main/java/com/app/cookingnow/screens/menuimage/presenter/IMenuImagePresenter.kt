package com.app.cookingnow.screens.menuimage.presenter

interface IMenuImagePresenter {

    fun getListMenuImage()
    fun onCreate()
}