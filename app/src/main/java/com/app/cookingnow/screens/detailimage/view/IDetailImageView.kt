package com.app.cookingnow.screens.detailimage.view

interface IDetailImageView {

    fun displayFullImage(url: String)

}