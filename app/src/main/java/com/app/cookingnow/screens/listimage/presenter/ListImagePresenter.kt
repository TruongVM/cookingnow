package com.app.cookingnow.screens.listimage.presenter

import android.os.Bundle
import android.view.View
import com.app.base.firebaseservice.FireBaseService
import com.app.cookingnow.screens.listimage.view.IListImageView
import com.app.cookingnow.utils.KeyDatabase

class ListImagePresenter(var bundle: Bundle, var iListImageView: IListImageView) : IListImagePresenter, FireBaseService.GetListImageSuccessListener {

    override fun loadData() {
        FireBaseService.getInstance().getDataDetailList(KeyDatabase.KEY_LIST_IMAGE, this)
        iListImageView.displayLoading(View.VISIBLE)
    }

    override fun onViewCreated() {
        loadData()
    }

    override fun onGetListImageSuccess() {
        iListImageView.displayResource(FireBaseService.getInstance().getListImage())
        iListImageView.displayLoading(View.GONE)
    }

}