package com.app.cookingnow.screens.listimage.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.app.base.extensions.inflate
import com.app.cookingnow.R
import com.app.cookingnow.screens.menuimage.model.ImageMenuModel
import com.app.cookingnow.utils.ImageUtil
import kotlinx.android.synthetic.main.item_list_image.view.*
import java.util.*

class ListImageAdapter(private var mContext: Context,
                       private var onItemClick: (eating: ImageMenuModel) -> Unit) : RecyclerView.Adapter<ListImageAdapter.ViewHolder>() {
    private var mListImageMenu: ArrayList<ImageMenuModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_list_image))
    }

    override fun getItemCount(): Int {
        return mListImageMenu.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onItemClick.invoke(mListImageMenu[position])
        }
        holder.fillData(mListImageMenu[position])
    }

    fun clearList() {
        mListImageMenu.clear()
    }

    fun updateList(data: ArrayList<ImageMenuModel>) {
        clearList()
        for (eating in data) {
            if (!mListImageMenu.contains(eating)) {
                mListImageMenu.add(eating)
            }
        }
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        fun fillData(imageMenuModel: ImageMenuModel) {
            ImageUtil.loadAvatarImage(mContext, imageMenuModel.avatar, itemView.imgImageForWallPaper)
            itemView.txtDownLoadTimesOfImage.text = imageMenuModel.download_times
        }
    }
}