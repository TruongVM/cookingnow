package com.app.cookingnow.screens.menuimage.view

import com.app.cookingnow.screens.menuimage.model.ImageMenuModel

interface IMenuImageView {
    fun setListMenuImage(listMenuImage: ArrayList<ImageMenuModel>)
    fun displayLoadingIcon(visibility: Int)
}