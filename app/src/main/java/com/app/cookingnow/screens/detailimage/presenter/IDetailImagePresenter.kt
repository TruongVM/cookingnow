package com.app.cookingnow.screens.detailimage.presenter

interface IDetailImagePresenter {

    fun onCreate()
    fun downloadImage()
    fun onStop()
}