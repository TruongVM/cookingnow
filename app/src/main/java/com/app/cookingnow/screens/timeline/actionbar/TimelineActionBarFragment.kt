package com.app.cookingnow.screens.timeline.actionbar

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.cookingnow.R

/**
 * Created by NTH1991 on 7/20/2018.
 */
class TimelineActionBarFragment:Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_timeline_action_bar, container, false)
    }
}