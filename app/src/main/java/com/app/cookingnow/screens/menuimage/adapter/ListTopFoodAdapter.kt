package com.app.cookingnow.screens.menuimage.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.app.base.extensions.inflate
import com.app.cookingnow.R
import com.app.cookingnow.screens.menuimage.model.ImageMenuModel
import com.app.cookingnow.utils.ImageUtil
import kotlinx.android.synthetic.main.item_list_top_eating.view.*
import java.util.*

class ListTopFoodAdapter(private var mContext: Context,
                         private var onItemClick: (eating: ImageMenuModel) -> Unit)
    : RecyclerView.Adapter<ListTopFoodAdapter.ViewHolder>() {
    private var mListTopEating: ArrayList<ImageMenuModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_list_top_eating))
    }

    override fun getItemCount(): Int {
        return mListTopEating.size
    }

    @SuppressLint("NewApi")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onItemClick.invoke(mListTopEating[position])
        }
        holder.fillData(mListTopEating[position])
    }

    fun clearList() {
        mListTopEating.clear()
        notifyDataSetChanged()
    }

    fun updateList(data: ArrayList<ImageMenuModel>) {
        for (eating in data) {
            if (!mListTopEating.contains(eating)) {
                mListTopEating.add(eating)
            }
        }
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        @RequiresApi(Build.VERSION_CODES.O)
        fun fillData(eatingModel: ImageMenuModel) {
            ImageUtil.loadAvatarImage(mContext, eatingModel.avatar, itemView.imgEatingAvatar)
            itemView.imgEatingAvatar.clipToOutline = true
            itemView.txtEatingName.text = eatingModel.image_name
//            itemView.txtCookingTime.text = eatingModel.cook_time
//            itemView.txtDifficultRate.text = eatingModel.difficult_rate
            itemView.txtCookingTimes.text = eatingModel.download_times
        }
    }
}
