package com.app.cookingnow.screens.main

import android.os.Bundle
import android.support.annotation.DrawableRes
import android.view.View
import android.widget.TabHost
import android.widget.TabWidget
import com.app.base.fragments.BaseFragment
import com.app.cookingnow.R
import com.app.cookingnow.screens.mypage.MyPageFragment
import com.app.cookingnow.screens.mypage.actionbar.MyPageActionBarFragment
import com.app.cookingnow.screens.notice.NoticeFragment
import com.app.cookingnow.screens.notice.actionbar.NoticeActionBarFragment
import com.app.cookingnow.screens.timeline.actionbar.TimelineActionBarFragment
import com.app.cookingnow.screens.menuimage.view.MenuImageFragment
import com.app.cookingnow.screens.menuimage.actionbar.TopActionBarFragment
import com.app.cookingnow.utils.TopTabs
import com.app.cookingnow.viewmodels.BaseViewModel
import com.app.cookingnow.views.ItemTabMainView
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : BaseFragment() {

    override fun getContentRes() = R.layout.fragment_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appBarLayout.addOnOffsetChangedListener { appBarLayout, verticalOffset ->
            val change = (StrictMath.abs(verticalOffset).toFloat() * 100) / appBarLayout.height
            val translation = (fragmentTabHost.height * change) / 100
            fragmentTabHost.translationY = translation
        }
        initTab()
    }

    override fun actionBack(): Boolean {
        return false
    }

    private fun initTab() {
        //content
        fragmentTabHost.setup(context!!, childFragmentManager, R.id.content_frame)
        fragmentTabHost.findViewById<TabWidget>(android.R.id.tabs).showDividers = 0
        fragmentTabHost.addTab(createTab(TopTabs.TOP, R.drawable.ic_menu_eating_selected, R.drawable.ic_menu_eating_not_selected), MenuImageFragment::class.java, null)
        fragmentTabHost.addTab(createTab(TopTabs.NOTICE, R.drawable.ic_downloaded_selected, R.drawable.ic_downloaded_not_selected), NoticeFragment::class.java, null)
        fragmentTabHost.addTab(createTab(TopTabs.MY_PAGE, R.drawable.ic_like_selected, R.drawable.ic_like_not_selected), MyPageFragment::class.java, null)
        fragmentTabHost.setOnTabChangedListener {
            actionBarTabHost.setCurrentTabByTag(
                    when (it) {
                        TopTabs.TOP -> TopTabs.TOP_ACTION_BAR
                        TopTabs.TIME_LINE -> TopTabs.TIME_LINE_ACTION_BAR
                        TopTabs.NOTICE -> TopTabs.NOTICE_ACTION_BAR
                        else -> TopTabs.MY_PAGE_ACTION_BAR
                    }
            )
            if (it != TopTabs.TOP)
                showActionBar()
        }

        //action bar
        actionBarTabHost.setup(context!!, childFragmentManager, android.R.id.tabcontent)
        actionBarTabHost.addTab(createActionBarTab(TopTabs.TOP_ACTION_BAR), TopActionBarFragment::class.java, null)
        actionBarTabHost.addTab(createActionBarTab(TopTabs.TIME_LINE_ACTION_BAR), TimelineActionBarFragment::class.java, null)
        actionBarTabHost.addTab(createActionBarTab(TopTabs.NOTICE_ACTION_BAR), NoticeActionBarFragment::class.java, null)
        actionBarTabHost.addTab(createActionBarTab(TopTabs.MY_PAGE_ACTION_BAR), MyPageActionBarFragment::class.java, null)

    }

    private fun createTab(tag: String, @DrawableRes selectedRes: Int, @DrawableRes unSelectedRes: Int): TabHost.TabSpec {
        val tab = fragmentTabHost.newTabSpec(tag)
        var indicatorTab = ItemTabMainView(context!!,
                selectedImgRes = selectedRes,
                unSelectedImgRes = unSelectedRes)
        tab.setIndicator(indicatorTab)
        return tab
    }

    private fun createActionBarTab(tag: String): TabHost.TabSpec {
        val tab = actionBarTabHost.newTabSpec(tag)
        tab.setIndicator(tag)
        return tab
    }

    override fun getFragmentViewModel(): BaseViewModel? {
        return null
    }

    fun showActionBar() {
        appBarLayout.setExpanded(true, true)
        fragmentTabHost.visibility = View.VISIBLE
    }

}