package com.app.cookingnow.screens.menuimage.view

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.app.base.fragments.BaseFragment
import com.app.base.transaction.StyleAnimation
import com.app.cookingnow.R
import com.app.cookingnow.screens.listimage.model.ImageModel
import com.app.cookingnow.screens.listimage.view.ListImageFragment
import com.app.cookingnow.screens.menuimage.adapter.MenuImageAdapter
import com.app.cookingnow.screens.menuimage.adapter.ListTopFoodAdapter
import com.app.cookingnow.screens.menuimage.model.ImageMenuModel
import com.app.cookingnow.screens.menuimage.presenter.IMenuImagePresenter
import com.app.cookingnow.screens.menuimage.presenter.MenuImagePresenter
import com.app.cookingnow.utils.LogUtils
import com.app.cookingnow.viewmodels.BaseViewModel
import kotlinx.android.synthetic.main.fragment_menu_image.*


class MenuImageFragment : BaseFragment(), IMenuImageView {

    override fun getFragmentViewModel(): BaseViewModel? = null

    override fun getContentRes() = R.layout.fragment_menu_image

    override var hasActionBar = true

    private lateinit var mMenuImageAdapter: MenuImageAdapter

    private lateinit var mListLatestEating: java.util.ArrayList<ImageMenuModel>

    private lateinit var mMenuImagePresenter: IMenuImagePresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mMenuImageAdapter = MenuImageAdapter(activity!!, ::onItemLatestEatingClick)

        rclListMenuImage.adapter = mMenuImageAdapter
        rclListMenuImage.setLayoutManager(GridLayoutManager(activity, 2))
        mMenuImagePresenter = MenuImagePresenter(this)
        mMenuImagePresenter.onCreate()

        swipeRefreshListMenuImage.setOnRefreshListener {
            LogUtils.d("setOnRefreshListener")
            mMenuImagePresenter.getListMenuImage()
        }

    }

    override fun actionBack(): Boolean {
       return false
    }

    override fun setListMenuImage(listMenuImage: ArrayList<ImageMenuModel>) {
        mListLatestEating = listMenuImage
        LogUtils.d("data "+listMenuImage)
        mMenuImageAdapter.updateList(listMenuImage)
    }

    override fun displayLoadingIcon(visibility: Int) {
        loadingProgress.visibility = visibility
        swipeRefreshListMenuImage.isRefreshing = false
    }


    private fun onItemLatestEatingClick(imageModel: ImageMenuModel) {
        transactionManager!!.replaceFragment(ListImageFragment.newInstance(imageModel), isAddBackStack = true, styleAnimation = StyleAnimation.SLIDE_FROM_RIGHT)
    }

}