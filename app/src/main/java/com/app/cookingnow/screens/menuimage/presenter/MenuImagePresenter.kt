package com.app.cookingnow.screens.menuimage.presenter

import android.view.View
import com.app.base.firebaseservice.FireBaseService
import com.app.cookingnow.screens.menuimage.view.IMenuImageView
import com.app.cookingnow.utils.KeyDatabase

class MenuImagePresenter(var listEatingView: IMenuImageView) : IMenuImagePresenter, FireBaseService.FirebaseSuccessListener {
    override fun getListMenuImage() {
        FireBaseService.getInstance().getDataLatestEating(KeyDatabase.KEY_LATEST_EATING, this)
        listEatingView.displayLoadingIcon(View.VISIBLE)
    }

    override fun onCreate() {
        getListMenuImage()
    }

    override fun onGetListLatestSuccess() {
        listEatingView.setListMenuImage(FireBaseService.getInstance().getListLatestEating())
        listEatingView.displayLoadingIcon(View.GONE)
    }
}