package com.app.cookingnow.screens.foodperform.view

import android.os.Bundle
import android.view.View
import com.app.base.fragments.BaseFragment
import com.app.cookingnow.R
import com.app.cookingnow.viewmodels.BaseViewModel
import kotlinx.android.synthetic.main.layout_action_bar_only_back.*
import kotlinx.android.synthetic.main.tab_bottom_perform_food.*

class PerformFragment : BaseFragment(), View.OnClickListener {
    override fun getFragmentViewModel(): BaseViewModel? = null

    override fun getContentRes(): Int {
        return R.layout.fragment_perform
    }

    override fun actionBack(): Boolean {
        return false
    }

    companion object {
        fun newInstance(): PerformFragment {
            val fragment = PerformFragment()
            val bundle = Bundle()

            fragment.arguments = bundle
            return fragment
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvTitle.text = getString(R.string.do_it)
    }

    private fun initTab() {
        tabHostFoodPerform.setup()
        val tabDescription = tabHostFoodPerform.newTabSpec("tag1")
        tabDescription.setContent(R.id.tab1)
        tabDescription.setIndicator(getString(R.string.detail_food_do_it))
        tabHostFoodPerform.addTab(tabDescription)
        val tabResource = tabHostFoodPerform.newTabSpec("tag2")
        tabResource.setContent(R.id.tab2)
        tabResource.setIndicator(getString(R.string.food_perform_finish))
        tabHostFoodPerform.addTab(tabResource)
    }

    override fun onClick(v: View?) {
        when(v){
            btnBack->{
                backActionHandle()
            }
        }
    }

}