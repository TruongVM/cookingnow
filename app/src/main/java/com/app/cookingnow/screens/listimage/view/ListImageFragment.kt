package com.app.cookingnow.screens.listimage.view

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.app.base.fragments.BaseFragment
import com.app.base.transaction.StyleAnimation
import com.app.cookingnow.R
import com.app.cookingnow.screens.detailimage.view.DetailImageFragment
import com.app.cookingnow.screens.listimage.adapter.ListImageAdapter
import com.app.cookingnow.screens.listimage.model.ImageModel
import com.app.cookingnow.screens.listimage.presenter.ListImagePresenter
import com.app.cookingnow.screens.listimage.presenter.IListImagePresenter
import com.app.cookingnow.screens.menuimage.model.ImageMenuModel
import com.app.cookingnow.utils.LogUtils
import com.app.cookingnow.viewmodels.BaseViewModel
import kotlinx.android.synthetic.main.fragment_list_image.*
import kotlinx.android.synthetic.main.layout_action_bar_only_back.*

class ListImageFragment : BaseFragment(), IListImageView, View.OnClickListener {
    private lateinit var mPresenter: IListImagePresenter
    private lateinit var mListResourceAdapter: ListImageAdapter

    override var hasActionBar = true

    override fun getContentRes(): Int {
        return R.layout.fragment_list_image
    }

    override fun getFragmentViewModel(): BaseViewModel? = null

    override fun actionBack(): Boolean {
        return false
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mListResourceAdapter = ListImageAdapter(activity!!, ::onItemImageClick)
        arguments?.let {
            mPresenter = ListImagePresenter(it, this)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPresenter.onViewCreated()
        rclListImage.adapter = mListResourceAdapter
        rclListImage.setLayoutManager(GridLayoutManager(activity, 2))
        btnBack.setOnClickListener(this)

        swipeRefreshListImage.setOnRefreshListener {
            mPresenter.loadData()
        }
    }

    companion object {
        fun newInstance(imageMenuModel: ImageMenuModel): ListImageFragment {
            val fragment = ListImageFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }

    }

    override fun displayResource(imageMenuModel: ArrayList<ImageMenuModel>) {
        mListResourceAdapter.updateList(imageMenuModel)
    }

    override fun onClick(v: View?) {
        when (v) {
            btnBack -> {
                backActionHandle()
            }

        }
    }

    override fun displayLoading(visibility: Int) {
        loadingProgressIfListImage.visibility = visibility
        swipeRefreshListImage.isRefreshing = false
    }

    private fun onItemImageClick(latestEatingModel: ImageMenuModel) {
        transactionManager!!.replaceFragment(DetailImageFragment.newInstance(latestEatingModel), isAddBackStack = true, styleAnimation = StyleAnimation.SLIDE_FROM_RIGHT)
    }

}