package com.app.cookingnow.screens.detailimage.presenter

import android.content.Context
import android.os.AsyncTask
import android.widget.Toast
import com.app.base.firebaseservice.FireBaseService
import com.app.cookingnow.utils.ImageUtil
import com.app.cookingnow.utils.LogUtils

class DownLoadAsyncTask(var mContext: Context) : AsyncTask<String, Int, Void>() {


    override fun doInBackground(vararg params: String?): Void? {
        ImageUtil.downloadImage(ImageUtil.convertStringToBitmap(FireBaseService.getInstance().getImageString(0)))
        return null
    }

    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        LogUtils.d("onPostExecute")
        Toast.makeText(mContext, "Download finish", Toast.LENGTH_LONG).show()
    }

    override fun onPreExecute() {
        super.onPreExecute()
        LogUtils.d("onPreExecute")
        Toast.makeText(mContext, "Downloading...", Toast.LENGTH_SHORT).show()

    }


}