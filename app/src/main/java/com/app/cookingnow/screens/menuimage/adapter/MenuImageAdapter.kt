package com.app.cookingnow.screens.menuimage.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.app.base.extensions.inflate
import com.app.cookingnow.R
import com.app.cookingnow.screens.menuimage.model.ImageMenuModel
import com.app.cookingnow.utils.ImageUtil
import kotlinx.android.synthetic.main.item_menu_image.view.*
import java.util.*

class MenuImageAdapter(private var mContext: Context,
                       private var onItemClick: (eating: ImageMenuModel) -> Unit)
    : RecyclerView.Adapter<MenuImageAdapter.ViewHolder>() {

    private var mListImageMenu: ArrayList<ImageMenuModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_menu_image))
    }

    @SuppressLint("NewApi")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onItemClick.invoke(mListImageMenu[position])
        }
        holder.fillData(mListImageMenu[position])
    }

    override fun getItemCount(): Int {
        return mListImageMenu.size
    }

    fun clearList() {
        mListImageMenu.clear()
        notifyDataSetChanged()
    }

    fun updateList(data: ArrayList<ImageMenuModel>) {
        for (eating in data) {
            if (!mListImageMenu.contains(eating)) {
                mListImageMenu.add(eating)
            }
        }
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        @RequiresApi(Build.VERSION_CODES.O)
        fun fillData(latestEatingModel: ImageMenuModel) {
            ImageUtil.loadAvatarImage(mContext, latestEatingModel.avatar, itemView.imgLatestEatingAvatar)
            itemView.imgLatestEatingAvatar.clipToOutline = true
            itemView.txtDownLoadTimes.text = latestEatingModel.download_times
            itemView.txtLatestEatingName.text = latestEatingModel.image_name
        }
    }
}