package com.app.cookingnow.screens.listimage.view

import com.app.cookingnow.screens.menuimage.model.ImageMenuModel

interface IListImageView {
    fun displayResource(imageMenuModel: ArrayList<ImageMenuModel>)
    fun displayLoading(visibility: Int)
}