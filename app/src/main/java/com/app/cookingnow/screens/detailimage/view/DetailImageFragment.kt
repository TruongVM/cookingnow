package com.app.cookingnow.screens.detailimage.view

import android.os.Bundle
import android.view.View
import com.app.base.fragments.BaseFragment
import com.app.cookingnow.R
import com.app.cookingnow.screens.detailimage.presenter.DetailImagePresenter
import com.app.cookingnow.screens.detailimage.presenter.IDetailImagePresenter
import com.app.cookingnow.screens.menuimage.model.ImageMenuModel
import com.app.cookingnow.utils.ImageUtil
import com.app.cookingnow.utils.KeyDatabase
import com.app.cookingnow.utils.LogUtils
import com.app.cookingnow.viewmodels.BaseViewModel
import kotlinx.android.synthetic.main.fragment_detail_image.*
import kotlinx.android.synthetic.main.layout_action_bar_expand.*
import kotlinx.android.synthetic.main.layout_action_bar_only_back.btnBack

class DetailImageFragment : BaseFragment(), IDetailImageView, View.OnClickListener {

    private lateinit var mPresenter: IDetailImagePresenter
    override fun getFragmentViewModel(): BaseViewModel? = null

    override fun getContentRes(): Int {
        return R.layout.fragment_detail_image
    }

    override fun actionBack(): Boolean {
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mPresenter = DetailImagePresenter(it, this, activity!!.applicationContext)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPresenter.onCreate()
        btnBack.setOnClickListener(this)
        btnExpand.setOnClickListener(this)
    }

    override fun onStop() {
        super.onStop()
        mPresenter.onStop()
    }


    companion object {
        fun newInstance(latestEatingModel: ImageMenuModel): DetailImageFragment {
            val fragment = DetailImageFragment()
            val bundle = Bundle()
            bundle.putString(KeyDatabase.KEY_AVATAR, latestEatingModel.avatar)
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun displayFullImage(url: String) {
        LogUtils.d("avatar= " + url)
        ImageUtil.loadAvatarImage(activity!!.applicationContext, url, imgFullImage)
    }

    override fun onClick(v: View?) {
        when (v) {
            btnBack -> {
                backActionHandle()
            }

            btnExpand -> {
                mPresenter.downloadImage()
            }

        }
    }
}