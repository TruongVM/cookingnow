package com.app.cookingnow.screens.timeline.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.app.cookingnow.R
import com.app.cookingnow.utils.TimelinePages
import kotlinx.android.synthetic.main.empty_view_followers.view.*

class EmptyView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0): LinearLayout(context, attrs, defStyleAttr){

    private var onClickSuggest : IClickSuggest? = null
    fun inflateView(type: Int){
        when(type){
            TimelinePages.FOLLOWER -> {
                LayoutInflater.from(context).inflate(R.layout.empty_view_followers,this)
                showSuggest.setOnClickListener{
                    onClickSuggest?.let {
                        it.onSuggest()
                    }
                }
            }
            TimelinePages.MY_POST -> LayoutInflater.from(context).inflate(R.layout.empty_view_my_post,this)
        }
    }

    fun setOnSuggest(onclick: IClickSuggest){
        onClickSuggest = onclick
    }
}