package com.app.cookingnow.screens.timeline.custom

interface IClickSuggest {
    fun onSuggest()
}