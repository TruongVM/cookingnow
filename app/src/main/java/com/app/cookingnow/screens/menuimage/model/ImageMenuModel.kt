package com.app.cookingnow.screens.menuimage.model

data class ImageMenuModel(var imageId: String = "",
                          var image_name: String = "",
                          var download_times: String = "",
                          var avatar: String = "") {


    override fun equals(other: Any?): Boolean {
        if (other is ImageMenuModel) {
            return imageId == other.imageId
        }
        return false
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}