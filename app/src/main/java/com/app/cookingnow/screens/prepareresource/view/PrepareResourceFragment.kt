package com.app.cookingnow.screens.prepareresource.view

import android.os.Bundle
import com.app.base.fragments.BaseFragment
import com.app.cookingnow.R
import com.app.cookingnow.viewmodels.BaseViewModel

class PrepareResourceFragment : BaseFragment() {

    override fun getFragmentViewModel(): BaseViewModel? = null

    override fun getContentRes(): Int {
        return R.layout.fragment_prepare_resoursce
    }

    override fun actionBack(): Boolean {
        return false
    }

    companion object {
        fun newInstance(): PrepareResourceFragment {
            val fragment = PrepareResourceFragment()
            val bundle = Bundle()

            fragment.arguments = bundle
            return fragment
        }

    }
}