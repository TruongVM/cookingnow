package com.app.cookingnow.screens.mypage.actionbar

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.cookingnow.R

/**
 * Created by NTH1991 on 7/20/2018.
 */
class MyPageActionBarFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_my_page_action_bar, container, false)
    }
}