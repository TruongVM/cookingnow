package com.app.cookingnow.views

import android.content.Context
import android.support.v7.widget.AppCompatEditText
import android.util.AttributeSet


class FontEditText @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatEditText(context, attrs, defStyleAttr) {

}