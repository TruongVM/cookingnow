package com.app.cookingnow.views

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.widget.LinearLayout
import com.app.cookingnow.R
import kotlinx.android.synthetic.main.layout_take_picture_bottom_sheet.view.*

@SuppressLint("ViewConstructor")

class BottomSheetTakePictureView(context: Context, var listener: BottomSheetListener?) : LinearLayout(context) {

    init {
        orientation = VERTICAL
        val padding = context.resources.getDimensionPixelSize(R.dimen.space_small)
        setPadding(padding, padding, padding, padding)
        setBackgroundResource(android.R.color.white)
        View.inflate(context, R.layout.layout_take_picture_bottom_sheet, this)

        lay_open_camera.setOnClickListener { listener?.doOpenCamera() }
        lay_open_gallery.setOnClickListener { listener?.doOpenGallery() }
    }

    interface BottomSheetListener {
        fun doOpenGallery()

        fun doOpenCamera()
    }
}