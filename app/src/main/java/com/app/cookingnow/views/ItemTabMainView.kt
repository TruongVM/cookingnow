package com.app.cookingnow.views

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView


class ItemTabMainView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, var selectedImgRes: Int? = null, var unSelectedImgRes: Int? = null
) : ImageView(context, attrs, defStyleAttr) {
    init {
        if (isSelected) {
            selectedImgRes?.let { setImageResource(it)
            }
        } else {
            unSelectedImgRes?.let { setImageResource(it) }
        }
    }

    override fun dispatchSetSelected(selected: Boolean) {
        super.dispatchSetSelected(selected)
        if (selected) {
            selectedImgRes?.let { setImageResource(it) }
        } else {
            unSelectedImgRes?.let { setImageResource(it) }
        }
    }
}