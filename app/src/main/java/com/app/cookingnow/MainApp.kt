package com.app.cookingnow

import android.app.Application
import com.app.cookingnow.api.Api
import com.app.cookingnow.modules.ApiClientModule
import com.app.cookingnow.modules.OkHttpClientModule
import com.app.cookingnow.modules.PicassoModule
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient



class MainApp : Application() {

    lateinit var api: Api
    lateinit var picasso: Picasso

    override fun onCreate() {
        super.onCreate()
        instance = this
        val okHttpClientModule = OkHttpClientModule()
        val file = okHttpClientModule.file(this)
        val okHttpClient = okHttpClientModule.okHttpClient(okHttpClientModule.cache(file), okHttpClientModule.httpLoggingInterceptor())
        initApi(okHttpClient)
        initPicasso(okHttpClient)
    }

    private fun initApi(okHttpClient: OkHttpClient) {
        val apiClientModule = ApiClientModule()
        val retrofit = apiClientModule.retrofit(okHttpClient, apiClientModule.gsonConverterFactory(GsonBuilder().create()))
        api = apiClientModule.apiClient(retrofit)
    }

    private fun initPicasso(okHttpClient: OkHttpClient) {
        val picassoModule = PicassoModule()
        picasso = picassoModule.picasso(picassoModule.okHttpDownloader(okHttpClient))
    }

    fun createApiLongTimeOut(): Api {
        val okHttpClientModule = OkHttpClientModule()
        val file = okHttpClientModule.file(this)
        val okHttpClient = okHttpClientModule.okHttpClient(okHttpClientModule.cache(file), okHttpClientModule.httpLoggingInterceptor(), true)
        val apiClientModule = ApiClientModule()
        val retrofit = apiClientModule.retrofit(okHttpClient, apiClientModule.gsonConverterFactory(GsonBuilder().create()))

        return apiClientModule.apiClient(retrofit)
    }

    companion object {
        lateinit var instance: MainApp
            private set
    }
}
