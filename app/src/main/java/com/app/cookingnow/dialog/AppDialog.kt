package com.app.cookingnow.dialog

import android.app.Dialog
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.cookingnow.R
import kotlinx.android.synthetic.main.dialog_common.*

class AppDialog : DialogFragment() {
    companion object {
        const val ARGS_BUILDER = "args_builder"
        const val BUTTON_RIGHT = 0
        const val BUTTON_LEFT = 1
    }

    private lateinit var builder: Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        builder = arguments?.getParcelable(ARGS_BUILDER)!!
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        val window = dialog.window
        window!!.setBackgroundDrawableResource(android.R.color.transparent)
        window.setDimAmount(0.5f)
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_common, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvContent.text = builder.content
        if (!builder.btnLeft.isNullOrBlank())
            btnLeft.text = builder.btnLeft
        else
            btnLeft.setText(R.string.common_ok_txt)
        btnLeft.setOnClickListener { buttonAction(BUTTON_LEFT) }
        if (!builder.btnRight.isNullOrBlank()) {
            btnRight.text = builder.btnRight
            btnRight.setOnClickListener { buttonAction(BUTTON_LEFT) }
        }
    }

    private fun buttonAction(which: Int) {
        if (builder.requestCode == -1) {
            val fragment = parentFragment
            var handle = false
            var callBack: DialogButtonClickListener
            if (fragment is DialogButtonClickListener) {
                callBack = fragment
                handle = callBack.onClickDialogButton(this, builder.requestCode, which)
            }
            if (handle) return
            val activity = activity
            if (activity is DialogButtonClickListener) {
                callBack = activity
                handle = callBack.onClickDialogButton(this, builder.requestCode, which)
            }
            if (handle) return
        }
        dismiss()

    }

    class Builder() : Parcelable {
        var content: String? = null
            private set
        var btnLeft: String? = ""
            private set
        var btnRight: String? = ""
            private set
        var requestCode = -1
            private set

        constructor(parcel: Parcel) : this() {
            content = parcel.readString()
            btnLeft = parcel.readString()
            btnRight = parcel.readString()
        }

        fun setContent(content: String): Builder {
            this.content = content
            return this
        }

        fun setTextBtnLeft(text: String): Builder {
            btnLeft = text
            return this
        }

        fun setTextBtnRight(text: String): Builder {
            btnRight = text
            return this
        }

        fun setRequestCode(code: Int): Builder {
            this.requestCode = code
            return this
        }

        fun build(): AppDialog {
            return AppDialog().apply {
                arguments = Bundle().apply { putParcelable(ARGS_BUILDER, this@Builder) }
            }
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(content)
            parcel.writeString(btnLeft)
            parcel.writeString(btnRight)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Builder> {
            override fun createFromParcel(parcel: Parcel): Builder {
                return Builder(parcel)
            }

            override fun newArray(size: Int): Array<Builder?> {
                return arrayOfNulls(size)
            }
        }
    }

    interface DialogButtonClickListener {
        fun onClickDialogButton(dialog: DialogFragment, requestCode: Int, which: Int): Boolean
    }
}
