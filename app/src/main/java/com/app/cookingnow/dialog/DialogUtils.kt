package com.app.cookingnow.dialog

import android.content.Context
import android.support.v4.app.FragmentManager
import com.app.cookingnow.R
import com.app.cookingnow.api.ResponseCode


object DialogUtils {
    fun show(context: Context, fragmentManager: FragmentManager, code: Int) {
        val dialogBuilder = when (code) {
            ResponseCode.EMAIL_NOT_FOUND -> AppDialog.Builder().setContent(context.getString(R.string.email_not_exist))
            ResponseCode.WRONG_VERIFICATION_CODE -> AppDialog.Builder().setContent(context.getString(R.string.authen_code_wrong))
            ResponseCode.AUTHEN_CODE_EXPIRED -> AppDialog.Builder().setContent(context.getString(R.string.authen_code_wrong))
            else -> AppDialog.Builder().setContent(context.getString(R.string.common_unknown_error, code))
        }

        dialogBuilder.build().show(fragmentManager, null)
    }

    fun show (fragmentManager: FragmentManager, msg: String) {
        AppDialog.Builder().setContent(msg).build().show(fragmentManager, null)
    }
}