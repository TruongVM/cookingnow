package com.app.cookingnow.actionbar

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.base.interfaces.ActionBarActionHandle
import com.app.base.interfaces.ActionBarController
import com.app.cookingnow.R

/**
 * Created by NTH1991 on 7/5/2018.
 */
class OnlyBackActionBar(var actionBarActionHandle: ActionBarActionHandle) : ActionBarController {
    private var title: TextView? = null
    override fun getActionBarLayout(): Int = R.layout.layout_only_back_action_bar
    override fun findViews(parent: ViewGroup) {
        title = parent.findViewById(R.id.tvTitle)
        parent.findViewById<View>(R.id.btnBack).setOnClickListener { actionBarActionHandle.doBack() }
    }

    override fun setUpViews() {
        title?.text = actionBarActionHandle.getTitle()
    }

    override fun setTitle(title: String) {
        this.title?.text = title
    }

}