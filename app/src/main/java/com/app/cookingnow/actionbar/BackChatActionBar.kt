package com.app.cookingnow.actionbar

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.base.interfaces.ActionBarActionHandle
import com.app.base.interfaces.ActionBarController
import com.app.cookingnow.R
import com.app.cookingnow.utils.Action

/**
 * Created by NTH1991 on 8/1/2018.
 */
class BackChatActionBar(var actionBarActionHandle: ActionBarActionHandle) : ActionBarController {
    lateinit var title: TextView
    override fun getActionBarLayout() = R.layout.layout_back_chat_action_bar

    override fun findViews(parent: ViewGroup) {
        title = parent.findViewById(R.id.tvTitle)
        parent.findViewById<View>(R.id.btnBack).setOnClickListener { actionBarActionHandle.doBack() }
        parent.findViewById<View>(R.id.btnChat).setOnClickListener { actionBarActionHandle.doAction(Action.OPEN_CHAT) }
    }

    override fun setUpViews() {
        title.text = actionBarActionHandle.getTitle()
    }

    override fun setTitle(title: String) {
        this.title.text = title
    }

}