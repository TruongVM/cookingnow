package com.app.cookingnow.actionbar

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.base.interfaces.ActionBarActionHandle
import com.app.base.interfaces.ActionBarController
import com.app.cookingnow.R
import com.app.cookingnow.utils.Action

/**
 * Created by NTH1991 on 7/2/2018.
 */
class UpdateInfoActionBar(var actionBarActionHandle: ActionBarActionHandle) : ActionBarController {
    private lateinit var title: TextView
    private lateinit var btnDone: View

    override fun getActionBarLayout() = R.layout.layout_update_infor_action_bar

    override fun findViews(parent: ViewGroup) {
        title = parent.findViewById(R.id.tvTitle)
        btnDone = parent.findViewById(R.id.btnDone)
        parent.findViewById<View>(R.id.btnBack).setOnClickListener { actionBarActionHandle.doBack() }
        btnDone.setOnClickListener { actionBarActionHandle.doAction(Action.DONE) }
    }

    override fun setUpViews() {
        title.text = actionBarActionHandle.getTitle()
    }

    override fun setTitle(title: String) {
        this.title.text = title
    }

    fun setCanClickDone(canClick: Boolean) {
        btnDone.isEnabled = canClick
    }

}