package com.app.cookingnow.modules

import android.content.Context
import android.text.TextUtils
import com.app.cookingnow.BuildConfig
import com.app.cookingnow.preferences.PrefKey
import com.app.cookingnow.preferences.UserPref
import com.app.cookingnow.preferences.get
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import java.util.concurrent.TimeUnit



class OkHttpClientModule {
    private val AUTHORIZATION = "Authorization"
    fun okHttpClient(cache: Cache, httpLoggingInterceptor: HttpLoggingInterceptor, isLongTimout: Boolean = false): OkHttpClient {
        val OkHttpBuilder = OkHttpClient()
                .newBuilder()
        if (isLongTimout) {
            OkHttpBuilder.readTimeout(2, TimeUnit.MINUTES)
            OkHttpBuilder.connectTimeout(1, TimeUnit.MINUTES)
            OkHttpBuilder.writeTimeout(10, TimeUnit.MINUTES)
        }
        return OkHttpBuilder
                .cache(cache)
                .addInterceptor(httpLoggingInterceptor)
                .addNetworkInterceptor {
                    val original = it.request()
                    val builder = original.newBuilder().method(original.method(), original.body())
                    val token: String? = UserPref.instance()[PrefKey.USER_TOKEN]
                    if (!TextUtils.isEmpty(token))
                        builder.header(AUTHORIZATION, token)
                    val request = builder
                            .build()
                    return@addNetworkInterceptor it.proceed(request)
                }
                .build()
    }

    fun cache(cacheFile: File) = Cache(cacheFile, 10 * 1000 * 1000) //10 MB


    fun file(context: Context): File {
        val file = File(context.cacheDir, "HttpCache")
        file.mkdirs()
        return file
    }

    fun httpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return httpLoggingInterceptor
    }
}