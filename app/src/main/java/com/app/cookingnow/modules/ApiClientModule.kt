package com.app.cookingnow.modules

import com.app.cookingnow.BuildConfig
import com.app.cookingnow.api.Api
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors



class ApiClientModule {

    fun apiClient(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }

    fun retrofit(okHttpClient: OkHttpClient, gsonConverterFactory: GsonConverterFactory) =
            Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(BuildConfig.API_DOMAIN)
                    .callbackExecutor(Executors.newSingleThreadExecutor())
                    .addConverterFactory(gsonConverterFactory)
                    .build()

    fun gsonConverterFactory(gson: Gson) = GsonConverterFactory.create(gson)

}