package com.app.cookingnow.modules

import com.app.cookingnow.MainApp
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient


class PicassoModule {

    fun picasso(okHttp3Downloader: OkHttp3Downloader) =
            Picasso.Builder(MainApp.instance).downloader(okHttp3Downloader).build()

    fun okHttpDownloader(okHttpClient: OkHttpClient) = OkHttp3Downloader(okHttpClient)

}