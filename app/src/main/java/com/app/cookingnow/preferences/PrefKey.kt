package com.app.cookingnow.preferences


object PrefKey {
    const val PUSH_TOKEN                = "push.token"
    const val ACTION_BAR_HEI            = "action.bar.hei"
    const val USER_TOKEN                = "user.token"
    const val IS_TOP_TUTORIAL_SHOWED    = "top.tutorial.showed"
    const val REGISTER_USER_DATA        = "register.user.data"
    const val REGISTER_CHILD_DATA       = "register.child.data"
}