package com.app.cookingnow.activities

import android.content.Intent
import android.os.Bundle
import com.app.base.activities.BaseActivity
import com.app.base.transaction.StyleAnimation
import com.app.cookingnow.R
import com.app.cookingnow.screens.main.MainFragment
import com.app.cookingnow.service.DownloadService
import com.app.cookingnow.MainApp as app

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (getTransactionManager().fragmentActive == null) {
            getTransactionManager().replaceFragment(MainFragment(), isAddBackStack = false, styleAnimation = StyleAnimation.SLIDE_NONE)
        }
    }

    override fun getPlaceHolder() = R.id.content_frame

    private fun startService() {
        startService(Intent(applicationContext, DownloadService::class.java))
    }
}
